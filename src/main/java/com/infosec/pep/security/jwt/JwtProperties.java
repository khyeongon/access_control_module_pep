package com.infosec.pep.security.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import lombok.Data;

@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtProperties {

	private String secretKey = "it-is-a-secret-only-we-that-should-know";

	//validity in milliseconds
    private long validityInMs = 3600000; // 1h    

    public JwtProperties() {
    }

    public JwtProperties(String secretKey, long validityInMs) {
        this.secretKey = secretKey;
        this.validityInMs = validityInMs;
    }

    public String getSecretKey() {
        return this.secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public long getValidityInMs() {
        return this.validityInMs;
    }

    public void setValidityInMs(long validityInMs) {
        this.validityInMs = validityInMs;
    }

    public JwtProperties secretKey(String secretKey) {
        this.secretKey = secretKey;
        return this;
    }

    public JwtProperties validityInMs(long validityInMs) {
        this.validityInMs = validityInMs;
        return this;
    }

}