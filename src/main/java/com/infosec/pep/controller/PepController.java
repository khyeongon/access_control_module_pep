package com.infosec.pep.controller;

import org.casbin.jcasbin.main.Enforcer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PepController {

    @Autowired
    private Enforcer enforcer;

	@RequestMapping("/**")
    public String index(HttpServletRequest request) {
        String path = request.getRequestURI();
        String method = request.getMethod();
        return String.format("Good, You are Autorized, path = %s, method = %s", path, method);
    }
}
