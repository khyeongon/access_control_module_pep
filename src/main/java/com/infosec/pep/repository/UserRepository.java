package com.infosec.pep.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

import com.infosec.pep.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

}